<?php
namespace Sixdg\DynamicsCRMConnector\Services;

use Sixdg\DynamicsCRMConnector\Models\Entity;

/**
 * Created by JetBrains PhpStorm.
 * User: alan.hollis
 * Date: 12/08/13
 * Time: 15:31
 */
class EntityValidator
{
    private $validationErrors = [];
    private $metaData;

    /**
     * @param        $requestType
     * @param Entity $entity
     *
     * @throws \RuntimeException
     */
    public function validate($requestType, Entity $entity)
    {
        $this->metaData = $entity->getMetadataRegistry();

        switch ($requestType) {
            case "create":
                $this->validateCreateRequest($entity);
                break;
            case "update":
                $this->validateUpdateRequest($entity);
                break;
        }

        if (count($this->validationErrors) > 0) {
            throw new \RuntimeException(implode(' , ', $this->validationErrors));
        }
    }

    /**
     * @param Entity $entity
     *
     * @return bool
     */
    private function validateCreateRequest(Entity $entity)
    {
        foreach ($entity->getDataKeys() as $key) {
            if (!$this->metaData[$key]->getIsValidForCreate()) {
                $this->validationErrors[] = "$key is not valid for create";

                return false;
            }
        }

        return true;
    }

    /**
     * @param Entity $entity
     *
     * @return bool
     */
    private function validateUpdateRequest(Entity $entity)
    {
        foreach ($entity->getDataKeys() as $key) {
            if (!$this->metaData[$key]->getIsValidForUpdate()) {
                $this->validationErrors[] = "$key is not valid for update";

                return false;
            }
        }

        return true;
    }
}
