<?php

namespace Sixdg\DynamicsCRMConnector\Requests;

use DOMElement;
use Sixdg\DynamicsCRMConnector\Builders\RequestBuilder;

/**
 * Class DeleteRequest
 *
 * @package Sixdg\DynamicsCRMConnector\Requests
 */
class DeleteRequest extends AbstractSoapRequest
{

    protected $action = 'http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Delete';
    protected $to = 'XRMServices/2011/Organization.svc';
    protected $entityName;
    protected $entityId;

    /**
     * @param RequestBuilder $requestBuilder
     */
    public function __construct(RequestBuilder $requestBuilder)
    {
        $this->securityToken = $requestBuilder->getSecurityToken();

        parent::__construct($requestBuilder);
    }

    /**
     *   * @return mixed|string
     */
    public function getXML()
    {
        $retrievalRequest = new \DOMDocument();

        $request = $this->getEnvelope();
        $node = $retrievalRequest->importNode($request, true);
        $retrievalRequest->appendChild($node);

        return $retrievalRequest->saveXML();
    }

    /**
     * @return DOMElement
     */
    protected function getEnvelope()
    {
        $envelope = $this->getSoapEnvelope();
        $envelope->appendChild($this->getHeader());
        $envelope->appendChild($this->getBody());

        return $envelope;
    }

    /**
     * @return \Sixdg\DynamicsCRMConnector\Components\DOM\DOMElement
     */
    protected function getBody()
    {
        $entityName = $this->domHelper->createElement('entityName', $this->getEntityName());
        $entityId = $this->domHelper->createElement('id', $this->getEntityId());

        $retrievalNode = $this->domHelper->createElementNS($this->serviceNS, 'Delete');
        $retrievalNode->appendChild($entityName);
        $retrievalNode->appendChild($entityId);

        $body = $this->domHelper->createElement('s:Body');
        $body->appendChild($retrievalNode);

        return $body;
    }

    /**
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     *
     * @param  id                                                   $id
     * @return \Sixdg\DynamicsCRMConnector\Requests\RetrieveRequest
     */
    public function setEntityId($id)
    {
        $this->entityId = $id;

        return $this;
    }
}
